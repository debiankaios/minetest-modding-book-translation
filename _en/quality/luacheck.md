---
title: Automatic Error Checking
layout: default
root: ../..
idx: 8.2
description: Use LuaCheck to find errors
redirect_from: /en/chapters/luacheck.html
---

## Introduction <!-- omit in toc -->

In this chapter, you will learn how to use a tool called LuaCheck to automatically
scan your mod for any mistakes. This tool can be used in combination with your
editor to provide alerts to any mistakes.

- [Installing LuaCheck](#installing-luacheck)
  - [Windows](#windows)
  - [Linux](#linux)
- [Running LuaCheck](#running-luacheck)
- [Configuring LuaCheck](#configuring-luacheck)
  - [Troubleshooting](#troubleshooting)
- [Using with editor](#using-with-editor)

## Installing LuaCheck

### Windows

Simply download luacheck.exe from
[the Github Releases page](https://github.com/mpeterv/luacheck/releases).

### Linux

First, you'll need to install LuaRocks:

    sudo apt install luarocks

You can then install LuaCheck globally:

    sudo luarocks install luacheck

Check that it's installed with the following command:

    luacheck -v

## Running LuaCheck

The first time you run LuaCheck, it will probably pick up a lot of false
errors. This is because it still needs to be configured.

On Windows, open powershell or bash in the root folder of your project
and run `path\to\luacheck.exe .`

On Linux, run `luacheck .` whilst in the root folder of your project.

## Configuring LuaCheck

Create a file called .luacheckrc in the root of your project. This could be the
root of your game, modpack, or mod.

Put the following contents in it:

```lua
unused_args = false
allow_defined_top = true

globals = {
    "minetest",
}

read_globals = {
    string = {fields = {"split"}},
    table = {fields = {"copy", "getn"}},

    -- Builtin
    "vector", "ItemStack",
    "dump", "DIR_DELIM", "VoxelArea", "Settings",

    -- MTG
    "default", "sfinv", "creative",
}
```

Next, you'll need to test that it works by running LuaCheck. You should get a lot
fewer errors this time. Starting at the first error you get, modify the code to
remove the issue, or modify the configuration if the code is correct. See the list
below.

### Troubleshooting

* **accessing undefined variable foobar** - If `foobar` is meant to be a global,
  add it to `read_globals`. Otherwise, add any missing `local`s to the mod.
* **setting non-standard global variable foobar** - If `foobar` is meant to be a global,
  add it to `globals`. Remove from `read_globals` if present.
  Otherwise, add any missing `local`s to the mod.
* **mutating read-only global variable 'foobar'** - Move `foobar` from `read_globals` to
  `globals`, or stop writing to foobar.

## Using with editor

It is highly recommended that you find and install a plugin for your editor of choice
to show you errors without running a command. Most editors will likely have a plugin
available.

* **VSCode** - Ctrl+P, then paste: `ext install dwenegar.vscode-luacheck`
* **Sublime** - Install using package-control:
        [SublimeLinter](https://github.com/SublimeLinter/SublimeLinter),
        [SublimeLinter-luacheck](https://github.com/SublimeLinter/SublimeLinter-luacheck).
